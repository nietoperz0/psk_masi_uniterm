import { useState } from 'react'
import styles from './ControlsPanel.module.css'

const ControlsPanel = (props: any) => {
	const [inputX, setInputX] = useState('')
	const [inputY, setInputY] = useState('')
	const [inputA, setInputA] = useState('')
	const [inputB, setInputB] = useState('')

	const onSekwButtonHandler = () => {
		if (inputX === '' || inputY === '') {
			return
		}
		props.onSekwHandlder({ A: inputX, B: inputY })
	}
	const onElButtonHandler = () => {
		if (inputA === '' || inputB === '') {
			return
		}
		props.onElHandler({ A: inputA, B: inputB })
	}

	const onChangeFirstHandler = () => {
		props.onChangeFirstHandler()
	}

	const onChangeSecondHandler = () => {
		props.onChangeSecondHandler()
	}

	const onResetFormHandler = () => {
		props.onResetFormHandler()
	}

	return (
		<>
			<div className={styles.controlsPanel}>
				<div className={styles.panels}>
					<div>
						<form className={styles.card}>
							<label>
								<p>Wyrażenie A</p>
								<input id='unitermA' name='unitermA' onChange={e => setInputX(e.target.value)}></input>
							</label>
							<label>
								<p>Wyrażenie B</p>
								<input id='unitermB' name='unitermB' onChange={e => setInputY(e.target.value)}></input>
							</label>
							<button className={styles.btn} type='button' onClick={onSekwButtonHandler}>
								Dodaj op. sekwencjonowania
							</button>
						</form>
						<form className={styles.card}>
							<label>
								<p>Wyrażenie A</p>
								<input id='unitermA' name='unitermA' onChange={e => setInputA(e.target.value)}></input>
							</label>
							<label>
								<p>Wyrażenie B</p>
								<input id='unitermB' name='unitermB' onChange={e => setInputB(e.target.value)}></input>
							</label>
							<button className={styles.btn} type='button' onClick={onElButtonHandler}>
								Dodaj op. eliminacji
							</button>
						</form>
						<div className={styles.card}>
							<button className={styles.btn} type='button' onClick={onChangeFirstHandler}>
								Zamien za 1 wartość
							</button>
							<button className={styles.btn} type='button' onClick={onChangeSecondHandler}>
								Zamien za 2 wartość
							</button>
							<button className={`${styles.btn} ${styles.clean}`} type='button' onClick={onResetFormHandler}>
								Wyczyść
							</button>
							<button
								className={styles.btn}
								type='button'
								onClick={() => {
									props.onSaveHandler()
								}}>
								Zapisz
							</button>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default ControlsPanel
