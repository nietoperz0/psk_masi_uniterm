import styles from './DrawPanel.module.css'
import OpEl from './OpEl'
import OpSekw from './OpSekw'

const DrawPanel = (props: any) => {
	return (
		<>
			<div className={styles.drawPanel}>
				<div className={styles.drawBackground}>
					{!props.opSekw.A && !props.opEl.A && (
						<div className={`${styles.notification}`}>
							<p>Aby rozpocząć wprowadź wymagane dane!</p>
						</div>
					)}
					<div className={styles.uniterms}>
						<div>
							<OpSekw opSekw={props.opSekw} opEl={props.opEl} position={props.position} connect={props.connect} />
						</div>
						{!props.connect && (
							<div style={{ marginTop: 150, marginLeft: -51 }}>
								<OpEl opEl={props.opEl} />
							</div>
						)}
					</div>
				</div>
			</div>
		</>
	)
}

export default DrawPanel
