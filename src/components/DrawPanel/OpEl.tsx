import styles from './OpEl.module.css'

const OpEl = (props: any) => {
	return (
		<>
			{props.opEl.A && props.opEl.B && (
				<div className={`${styles.opEl} ${props.position ? styles.values__onRight : ''}`}>
					<div className={styles.parabolaVertical}>
						<div className={`${styles.values}`}>
							<div className={styles.firstValue}>{props.opEl.A}</div>
							<div className={styles.spacerValue}>;</div>
							<div className={styles.endValue}>{props.opEl.B}</div>
						</div>
					</div>
				</div>
			)}
		</>
	)
}

export default OpEl
