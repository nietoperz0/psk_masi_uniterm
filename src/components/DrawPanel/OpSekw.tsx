import styles from './OpSekw.module.css'
import OpEl from './OpEl'
import { useEffect, useRef, useState } from 'react'

const OpSekw = (props: any) => {
	const defaultWidth = 300

	const parabolaRef = useRef<HTMLDivElement | null>(null)
	const [parabolaWidth, setParabolaWidth] = useState(defaultWidth)

	useEffect(() => {
		setParabolaWidth(defaultWidth)
		if (props.opSekw.B.length >= 7) {
			setParabolaWidth(defaultWidth + (props.opSekw.B.length - 7) * 20)
		}
		if (props.opEl.A.length >= 7) {
			setParabolaWidth(defaultWidth + (props.opEl.A.length - 4) * 20)
		}
	}, [props.opSekw, props.opEl])

	return (
		<>
			<div className={styles.opSekw}>
				{props.opSekw.A && props.opSekw.B && (
					<div ref={parabolaRef} className={`${styles.parabolaHorizonatal}`} style={{ width: parabolaWidth }}>
						<div className={styles.values}>
							{props.connect && (
								<div className={styles.firstValue}>{props.position ? props.opSekw.A : <OpEl opEl={props.opEl} />}</div>
							)}
							{!props.connect && <div className={styles.firstValue}>{props.opSekw.A}</div>}
							<div className={styles.spacerValue}>;</div>
							<div className={styles.endValue}>{!props.position ? props.opSekw.B : <OpEl opEl={props.opEl} />}</div>
						</div>
					</div>
				)}
			</div>
		</>
	)
}

export default OpSekw
