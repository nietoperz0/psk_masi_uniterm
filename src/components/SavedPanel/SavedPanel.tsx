import { useState } from 'react'
import styles from './SavedPanel.module.css'
import SavedList from './SavedList/SavedList'

const SavedPanel = (props: any) => {
	const [isOpen, setIsOpen] = useState(false)
	const closeOpenBtnHandler = () => {
		setIsOpen(!isOpen)
	}

	return (
		<div>
			<div className={styles.openCloseBtn} onClick={closeOpenBtnHandler}>
				<i className='fa-solid fa-floppy-disk'></i>
			</div>
			<div className={`${styles.savedPanel} ${isOpen ? '' : styles.closed}`}>
				<SavedList saved={props.saved} onLoadHandler={props.onLoadHandler} onDeleteHandler={props.onDeleteHandler} />
			</div>
		</div>
	)
}

export default SavedPanel
