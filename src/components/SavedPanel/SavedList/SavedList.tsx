import styles from './SavedList.module.css'

const SavedList = (props: any) => {
	const onLoadHandler = (number: any) => {
		props.onLoadHandler(number.target.value)
	}

	const onDeleteHandler = (id: any) => {
		props.onDeleteHandler(id.target.value)
	}

	return (
		<div className={styles.list}>
			<ul>
				{props.saved.map((item: any, index: number) => {
					return (
						<li key={index}>
							<span>{item.name}</span>
							<div>
								<button value={index} onClick={onLoadHandler}>
									<div>
										<i className='fa-solid fa-pencil'></i>
									</div>
								</button>
								<button value={index} onClick={onDeleteHandler}>
									<div>
										<i className='fa-solid fa-trash'></i>
									</div>
								</button>
							</div>
						</li>
					)
				})}
			</ul>
		</div>
	)
}

export default SavedList
