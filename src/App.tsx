import { useEffect, useState } from 'react'
import './App.css'
import ControlsPanel from './components/ControlsPanel/ControlsPanel'
import DrawPanel from './components/DrawPanel/DrawPanel'
import SavedPanel from './components/SavedPanel/SavedPanel'

interface IUniterm {
	name: string
	opSekw: { A: string; B: string }
	opEl: { A: string; B: string }
}

function App() {
	const [saved, setSaved] = useState<IUniterm[]>([
		{ name: 'Uniterm', opSekw: { A: 'x', B: 'y' }, opEl: { A: 'A', B: 'B' } },
	])
	const [position, setPosition] = useState<boolean>(false)
	const [connect, setConnect] = useState<boolean>(false)

	const [opSekw, setOpSekw] = useState<{ A: string; B: string }>({ A: '', B: '' })
	const [opEl, setOpEl] = useState<{ A: string; B: string }>({ A: '', B: '' })

	useEffect(() => {
		const loaded = localStorage.getItem('saved')
		if (loaded) {
			setSaved(JSON.parse(loaded))
		}
	}, [])

	const onSekwHandler = (data: any): void => {
		setOpSekw({ A: data.A, B: data.B })
	}

	const onElHandler = (data: any): void => {
		setOpEl({ A: data.A, B: data.B })
	}

	const onSaveHandler = (): void => {
		if (!opSekw.A || !opSekw.B || !opEl.A || !opEl.B) {
			alert('Nie podano wszystkich wartości')
			return
		}
		const saveName = prompt('Podaj nazwę aby zapisać')
		if (saveName) {
			const newSave = [...saved, { name: saveName, opSekw, opEl }]
			setSaved(newSave)
			localStorage.setItem('saved', JSON.stringify(newSave))
		} else {
			alert('Nie podano nazwy')
		}
	}

	const onDeleteHandler = (id: string): void => {
		let newSave = saved.filter((item, index) => index !== parseInt(id))
		setSaved(newSave)
		localStorage.setItem('saved', JSON.stringify(newSave))
	}

	const onLoadHandler = (number: number): void => {
		setOpSekw(saved[number].opSekw)
		setOpEl(saved[number].opSekw)
		setPosition(false)
		setConnect(false)
	}

	const onChangeFirstHandler = (): void => {
		setConnect(true)
		setPosition(false)
	}

	const onChangeSecondHandler = (): void => {
		setConnect(true)
		setPosition(true)
	}

	const onResetFormHandler = (): void => {
		setConnect(false)
		setPosition(false)
		setOpSekw({ A: '', B: '' })
		setOpEl({ A: '', B: '' })
	}

	return (
		<>
			<div className='app'>
				<SavedPanel saved={saved} onLoadHandler={onLoadHandler} onDeleteHandler={onDeleteHandler} />
				<DrawPanel opSekw={opSekw} opEl={opEl} position={position} connect={connect} />
				<ControlsPanel
					onSekwHandlder={onSekwHandler}
					onElHandler={onElHandler}
					onChangeFirstHandler={onChangeFirstHandler}
					onChangeSecondHandler={onChangeSecondHandler}
					onSaveHandler={onSaveHandler}
					onResetFormHandler={onResetFormHandler}
				/>
			</div>
		</>
	)
}

export default App
